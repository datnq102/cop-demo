package selenide.calculator;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.lang.reflect.Method;
import java.util.stream.Stream;

import static com.codeborne.selenide.Selenide.*;

@Tag("Calculator")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@DisplayNameGeneration(CalculatorTest.AddEnvironment.class)
public class CalculatorTest {

    static String CALCULATOR_PAGE_URL = "https://juliemr.github.io/protractor-demo/";
    String firstNumber = System.getProperty("calculator.firstNumber"),
            secondNumber = System.getProperty("calculator.secondNumber"),
            sum = System.getProperty("calculator.sum"),
            difference = System.getProperty("calculator.difference"),
            product = System.getProperty("calculator.product"),
            quotient = System.getProperty("calculator.quotient");

    @Test
    @Order(1)
    public void addNumbers() {
        open(CALCULATOR_PAGE_URL);
        new CalculatorPage().enterFirstNumber(firstNumber);
        new CalculatorPage().enterSecondNumber(secondNumber);
        new CalculatorPage().selectOperator("ADDITION");
        new CalculatorPage().clickGoButton();
        new CalculatorPage().resultElement().shouldBe(Condition.exactText(sum));
    }

    @Test
    @Order(2)
    public void subtractNumbers() {
        open(CALCULATOR_PAGE_URL);
        new CalculatorPage().enterFirstNumber(firstNumber);
        new CalculatorPage().enterSecondNumber(secondNumber);
        new CalculatorPage().selectOperator("SUBTRACTION");
        new CalculatorPage().clickGoButton();
        new CalculatorPage().resultElement().shouldBe(Condition.exactText(difference));
    }

    @Test
    @Order(3)
    public void multiplyNumbers() {
        open(CALCULATOR_PAGE_URL);
        new CalculatorPage().enterFirstNumber(firstNumber);
        new CalculatorPage().enterSecondNumber(secondNumber);
        new CalculatorPage().selectOperator("MULTIPLICATION");
        new CalculatorPage().clickGoButton();
        new CalculatorPage().resultElement().shouldBe(Condition.exactText(product));
    }

    @Test
    @Order(4)
    public void divideNumbers() {
        open(CALCULATOR_PAGE_URL);
        new CalculatorPage().enterFirstNumber(firstNumber);
        new CalculatorPage().enterSecondNumber(secondNumber);
        new CalculatorPage().selectOperator("DIVISION");
        new CalculatorPage().clickGoButton();
        new CalculatorPage().resultElement().shouldBe(Condition.exactText(quotient));
    }

    @Tag("Parameterized")
    @ParameterizedTest(name = "Should add numbers correctly ({0}, {1}, {2})")
    @MethodSource("numberProvider")
    public void addNumbersWithDataSets(int firstNumber, int secondNumber, int sum) {
        open(CALCULATOR_PAGE_URL);
        CalculatorPage calculatorPage = new CalculatorPage();
        calculatorPage.enterFirstNumber(firstNumber);
        calculatorPage.enterSecondNumber(secondNumber);
        calculatorPage.selectOperator("ADDITION");
        calculatorPage.clickGoButton();
        calculatorPage.resultElement().shouldBe(Condition.exactText(Integer.toString(sum)));
    }

    static Stream<Arguments> numberProvider() {
        return Stream.of(
                Arguments.of(1, 2, 3),
                Arguments.of(0, -1, -1),
                Arguments.of(-2, -3, -4)
        );
    }

    @Tag("MultipleBrowsers")
    @ParameterizedTest(name = "This test should run on multiple browsers - {0}")
    @ValueSource(strings = {"chrome", "firefox", "safari"})
    public void addNumbersOnMultipleBrowsers(String browser) {
        Configuration.browser = browser;
        open(CALCULATOR_PAGE_URL);
        new CalculatorPage().enterFirstNumber(firstNumber);
        new CalculatorPage().enterSecondNumber(secondNumber);
        new CalculatorPage().selectOperator("ADDITION");
        new CalculatorPage().clickGoButton();
        new CalculatorPage().resultElement().shouldBe(Condition.exactText(sum));
        closeWebDriver();
    }

    static class AddEnvironment extends DisplayNameGenerator.Standard {
        @Override
        public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
            return this.addEnv(testMethod);
        }

        String addEnv(Method testMethod) {
            return testMethod.getName() + " - " + Configuration.browser;
        }
    }

}
