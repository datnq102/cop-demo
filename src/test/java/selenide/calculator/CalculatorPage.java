package selenide.calculator;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class CalculatorPage {

    SelenideElement firstInput = $("[ng-model='first']"),
            secondInput = $("[ng-model='second']"),
            operator = $("[ng-model='operator']"),
            goButton = $("#gobutton"),
            result = $(By.xpath("(//tbody/tr/td[3])[1]"));

    @Step
    public void enterFirstNumber(String firstNumber) {
        firstInput.setValue(firstNumber);
    }

    @Step
    public void enterFirstNumber(int firstNumber) {
        firstInput.setValue(Integer.toString(firstNumber));
    }

    @Step
    public void enterSecondNumber(String secondNumber) {
        secondInput.setValue(secondNumber);
    }

    @Step
    public void enterSecondNumber(int secondNumber) {
        secondInput.setValue(Integer.toString(secondNumber));
    }

    @Step
    public void selectOperator(String operatorName) {
        operator.selectOptionByValue(operatorName);
    }

    @Step
    public void clickGoButton() {
        goButton.click();
    }

    @Step
    public SelenideElement resultElement() {
        return result;
    }

}
